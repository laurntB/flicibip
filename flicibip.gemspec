# coding: utf-8

require 'English'
require_relative 'lib/version.rb'

Gem::Specification.new do |spec|
  
  spec.name    = 'flicibip'
  spec.version = Flicibip::Version::REF
  spec.authors = ['laurent.beltran']
  spec.email   = ['laurent.beltran@laposte.net']
  spec.homepage = 'https://gitlab.com/laurntB/flicibip'

  spec.summary     = 'Fetching Layers Concerned By Parcel'
  spec.description = <<-STR
    Fetching features in some wfs layers (on Carmen) concerned by
    a target parcel on Martinique island. The parcel geometry is fetched from
    Cadastre API because no cadastre is available on Carmen.
  STR

  spec.license     = 'MIT'

  spec.files         = [
    'lib/flicibip.rb',
    'lib/version.rb',
    'lib/flicibip/logging.rb',
    'lib/flicibip/gml.rb',
    'lib/flicibip/json.rb',
    'lib/flicibip/parcelle.rb',
    'lib/flicibip/referentiel.rb',    
    'lib/flicibip/report.rb',
    'lib/flicibip/rgeo.rb',
    'lib/flicibip/rgjsn.rb',
    'lib/flicibip/avcptr.rb',
    'lib/flicibip/uri.rb',
    'lib/flicibip/providers.rb',
    'lib/flicibip/providers/generic_wfs.rb',
    'lib/flicibip/providers/cadastre.rb',
    'lib/flicibip/providers/carmen.rb',
    'lib/flicibip/providers/etalab.rb',
    'lib/flicibip/providers/geomartinique.rb',
    'lib/flicibip/providers/geoplateforme.rb',
    'lib/flicibip/providers/geoportail_urbanisme.rb',
    'lib/flicibip/providers/deal972lizmap.rb',
    'lib/flicibip/providers/local_geoserver.rb',
    'data/cacert.pem',
    'data/referentiel_couches.csv'
  ]

  spec.executables   = 'flicibip'

  spec.required_ruby_version = '~> 3'
  
  spec.add_dependency 'zlib', '~> 1.1'
  spec.add_dependency 'json', '~> 2.5'
  spec.add_dependency 'csv', '~> 3.1'
  spec.add_dependency 'tempfile', '~> 0.1'
  spec.add_dependency 'pathname', '~> 0.1'
  spec.add_dependency 'uri', '~> 0.10'
  spec.add_dependency 'net-http', '0.1.1'
  spec.add_dependency 'erb', '~> 2.2'
  spec.add_dependency 'logger', '~> 1.4'
  spec.add_dependency 'yaml', '~> 0.1'

  # spec.add_dependency 'ffi-geos'
  spec.add_dependency 'rgeo', '~> 3.0'
  spec.add_dependency 'rgeo-proj4', '~> 4.0'
  spec.add_dependency 'rgeo-geojson', '~> 2.2'
  spec.add_dependency 'nokogiri', '~> 1', '>= 1.12'
  
end
