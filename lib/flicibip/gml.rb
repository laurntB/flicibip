require "nokogiri"

module Readers

  module GML
    def self.read version, request_type, data
      case version
      when '2.0.0'
        gmlreader = GML_2_0_0.new(data)
      when '1.3.0'
        gmlreader = GML_1_3_0.new(data)
      else
        puts 'Reader for GML #{version} not implemented'
      end
      if gmlreader.public_methods.include? request_type.to_sym
        gmlreader.send(request_type)
      else
        p [version, request_type]
        puts '#{request_type} request type not implemented'
      end
    end
  end
  
  class GMLbase

    attr_reader :matched
    attr_reader :docxml

    def initialize string 
      @docxml = Nokogiri::XML(string)  ##gml_pth / File.open(gml_pth)      
    end
    
    def xpath path
      @docxml.xpath path, @docxml.namespaces
    end
    
    def bbox
      qrypath = "/wfs:FeatureCollection/wfs:boundedBy/gml:Envelope"
      nodecollection = self.xpath qrypath
      envelope_node = nodecollection[0]
      cn = envelope_node.first_element_child
      stack = [cn, ""]
      loop do
        stack = self.traverse_childs stack
        break if stack[0].nil?
      end
      stack[1].strip.gsub(" ",",")
    end

    

    def GetCapabilities
      summary = {}

      res = self.xpath("//ows:ServiceType/text()")
      summary['ServiceType'] = res.first.to_s

      
      res = self.xpath("//ows:ServiceTypeVersion/text()")
      summary['ServiceTypeVersion'] = res.first.to_s

      xpath_qry = "//ows:Parameter[@name='AcceptVersions']/ows:Value/text()"
      res = self.xpath xpath_qry
      values = res.map do |x| x.to_s end
      summary['acceptVersions'] = values

      res = self.xpath("//xmlns:FeatureType")
      values = res.to_h do
        |x|  [
          x.xpath('./xmlns:Name/text()', @docxml.namespaces).first
            .to_s, 
          x.xpath('./xmlns:Title/text()', @docxml.namespaces).first
            .to_s.gsub('\n',' ')
        ]
      end
      summary['CouchesDisponibles'] = values
      
      summary
      
    end
    
    def GetFeature
      dict = {}
      members = self.xpath("/wfs:FeatureCollection/wfs:member")
      members.each do |m|
        features =  m.xpath(
          "./wfs:FeatureCollection/wfs:member/child::*",
          @docxml.namespaces
        )
        if features.empty?
          features =  m.xpath("./child::*")
          typeName = (features.map {|e| e.name}).uniq
          fc = m.xpath("./wfs:FeatureCollection")
          if fc.empty?
            feature_nbr = features.length
          else
            feature_nbr = fc[0]["numberMatched"]
          end
          dict[typeName[0]] = feature_nbr 
        else
          typeName = (features.map {|e| e.name}).uniq
          fc = m.xpath("./wfs:FeatureCollection")
          if fc.empty?
            feature_nbr = features.length
          else
            feature_nbr = fc[0]["numberMatched"]
          end
          dict[typeName[0]] = feature_nbr 
        end
      end
      dict
    end


    def checklist_response
      refdict = Flicibip::Referentiel.as_dict
      refdict.each do |k,v|
        refdict[k][:nbr]=nil
        if @matched.has_key? k
          refdict[k][:nbr]=@matched[k]
        end
      end
      refdict
    end

    
    private
    
    def  traverse_childs a2e
      # p a2e
      node = a2e[0]
      nodenext = node
      newtext = a2e[1]
      if !node.nil?
        # p node.node_name
        # p node.text
        if node.node_name == "upperCorner" or node.node_name == "lowerCorner"
          newtext = newtext + " " + node.text
        end
        nodenext = node.next_sibling
      end
      [nodenext, newtext]
    end
  end


  class GML_1_3_0 < GMLbase

    def initialize string
      super(string)
    end
        
  end
  
  class GML_2_0_0 < GMLbase

    def initialize string
      super(string)
    end

  end
end
