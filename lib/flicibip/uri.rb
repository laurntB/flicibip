require 'uri'

# https://docs.ruby-lang.org/en/master/URI.html

module Flicibip

  class Param

    attr_reader :name, :value
    
    def self.parse string_value_name
      tarray = string_value_name.split('=')
      return Param.new(tarray[0],tarray[1])
    end

    def initialize name, value
      @name = self.filter_wfs_params(name)
      @value = value
    end

    def filter_wfs_params word
      wfs_query_param_words = [ # that I use
        'VERSION',
        'SERVICE',
        'REQUEST'
      ]
      if wfs_query_param_words.include? word.upcase
        word.upcase
      else
        word
      end
    end
  end


  
  class UrlQuery
    
    attr_reader :uri, :query_hash
    
    def initialize uri_string
      @uri = URI.parse(uri_string)
      @query_hash = self.build_query_hash
    end

    def build_query_hash
      @uri.query.split("&",-1).map{ |param|
        pparam = Param::parse(param)
        [pparam.name, pparam.value]
      }.to_h
    end

    def wfs?
      if @query_hash.key?('SERVICE')
        if @query_hash['SERVICE'].upcase == 'WFS'
          true
        else
          false
        end
      else
        false
      end
    end

    def wfs_version
      if self.wfs?
        if @query_hash.key? 'VERSION'
          @query_hash['VERSION']
        else
          nil
        end
      else
        nil
      end
    end
    
  end


  class WfsService
    

    attr_reader :baseurl, :service, :version
    
    def initialize baseurl, version=nil
      @baseurl = baseurl
      @service = "WFS"
      @version = version ? version : '2.0.0'
    end
    
    def to_s
      if @baseurl.include? '?'
        "#{@baseurl}&SERVICE=#{@service}&VERSION=#{@version}"
      else
        "#{@baseurl}?SERVICE=#{@service}&VERSION=#{@version}"
      end
    end

    def to_str
      self.to_s
    end

    def build_query
      WfsQuery.new(self)
    end

    def wfs_version
      @version
    end
  end

  
  class WfsQuery 
 

    attr_reader :service

    
    def initialize service
      self.default
      @service = service
    end

    def default
      @request = nil
      @typeNames = nil
      @bbox = nil
      @typeName = nil
      @srsName = "EPSG:32620"
    end
    
    def GetCapabilities
      @request = 'GetCapabilities'
      self
    end
    
    def GetFeature
      @request = 'GetFeature'
      self
    end

    def bbox bbox
      @bbox = bbox
      self
    end

    def typeNames typenames
      if @service.version == '2.0.0'
        @typeNames = typenames
      else
        puts "typeNames unavailable for WFS version #{@service.version}"
      end
      self
    end

    def typeName typename
      @typeName = typename
      self
    end    

    def srsName srsname
      @srsName = srsname
      self
    end    


    def params
      params_hash = {
        'REQUEST' => @request,
        'typeNames' => @typeNames,
        'bbox' => @bbox,
        'srsName' => @srsName,
        'typeName' => @typeName
      }
      params_hash.compact.to_a
    end

    def request_type
      @request
    end

    def monolayer?
      if @typeNames == nil && @typeName != nil
        true
      else
        false
      end
    end

    def target_layer
      @typeName
    end
    
    def to_str
      uri = URI(@service)
      in_params = URI.decode_www_form(uri.query)+self.params
      uri.query= URI.encode_www_form(in_params)
      uri.to_s
    end

    def to_s
      self.to_str
    end

    

    def wfs_version
      @service.version
    end
    
  end
end



