

class ParcelleName
  
  attr_reader :commune, :section, :numero ,:prefix
  
  def initialize commune, section, numero, prefix='000'
    @commune = commune
    @prefix = prefix
    @section = section.to_s.rjust(2,'0').upcase
    @numero = numero.to_s.rjust(4,'0')
  end

  def as_id
    @commune.to_s \
    + @prefix     \
    + @section    \
    + @numero
  end

  def ==(other)
    @commune == other.commune &&
      @section == other.section &&
      @numero  == other.numero 
  end

  def as_str
    @commune+"-"+@section+"-"+@numero
  end
end


class Parcelle

  attr_reader :parcellename, :geometry, :properties
  
  def initialize parcellename, geometry, properties
    @parcellename = parcellename
    @geometry = geometry
    @properties = properties 
  end
  
  def bbox
    RGeo::Cartesian::BoundingBox.create_from_geometry(@geometry)
  end  
end


