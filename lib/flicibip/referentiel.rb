require 'csv'

module Flicibip

  class Referentiel



    def self.table
      data_filepath = Pathname(__dir__)
                        .parent.parent + "data" + "referentiel_couches.csv"
      CSV.table(data_filepath)
    end

    
    def self.get_typenames
      self.table[:lizmapwfstn].compact.select{|item| !item.start_with?('?') }
    end

    
    def self.tnlist_as_str
      self.get_typenames.join(',')
    end

    
    def self.as_dict
      table = self.table
      table.by_row!
      sel = table.delete_if do |row|
        row[:lizmapwfstn].nil? or row[:lizmapwfstn].start_with?('?')
      end
      
      dict = {}
      sel.each do |row|
        dict[row[:lizmapwfstn]]={:libelle => row[:lizmapwfstitre]}
      end
      dict
    end

    
  end

end
