require 'logger'


module Logging

  # https://stackoverflow.com/questions/917566/ruby-share-logger-instance-among-module-classes

  # This is the magical bit that gets mixed into your classes
  def logger
    @logger ||= Logging.logger_for(self.class.name)
  end
  
  # Use a hash class-ivar to cache a unique Logger per class:
  @loggers = {}

  class << self
    def logger_for(classname)
      @loggers[classname] ||= configure_logger_for(classname)
    end

    def configure_logger_for(classname)
      output = nil #nil #STDOUT
      logger = Logger.new(output)
      logger.progname = classname
      logger
    end
  
  end
  
end
