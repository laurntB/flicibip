require "rgeo"
require "rgeo/proj4"
require "rgeo-geojson"


class RGeo::Cartesian::BoundingBox 
  def as_wfs_spec
    @min_x.to_s+\
    "," + @min_y.to_s+\
    "," + @max_x.to_s+\
    "," + @max_y.to_s  
  end
end


class RGeo::GeoJSON::FeatureCollection
  
  def get_crs
    geography = RGeo::Geos.factory(coord_sys: "EPSG:4326")
    projection = RGeo::Geos.factory(coord_sys: "EPSG:32620")
    {
      in: geography,
      out: projection
    }
  end
  
  def find_by parcellename
    matched = nil
    self.each do |feature|
      if feature.properties['id'] == parcellename.as_id
        matched = feature
        break
      end
    end
    matched
  end
  
  def extract_parcelle parcellename
    feature = self.find_by parcellename
    feature_wth_crs = RGeo::Feature.cast(
      feature.geometry, factory: self.get_crs[:in], project: true)
    reprojeted_feature = RGeo::Feature.cast(
      feature_wth_crs, factory: self.get_crs[:out], project: true)
    Parcelle.new(parcellename, reprojeted_feature, feature.properties)
  end
end


def reproject_bounds spec_bounds
  coords = spec_bounds.split(',').map(&:to_f)
  
  geography  = RGeo::Geos.factory(coord_sys: "EPSG:4326")
  point_a = geography.point(coords[0],coords[1])
  point_b = geography.point(coords[2],coords[3])
  
  projection = RGeo::Geos.factory(coord_sys: "EPSG:32620")
  reproj_point_a = RGeo::Feature.cast(
    point_a, factory: projection, project: true
  )
  reproj_point_b = RGeo::Feature.cast(
    point_b, factory: projection, project: true
  )
  
  "#{reproj_point_a.x},#{reproj_point_a.y},#{reproj_point_b.x},#{reproj_point_b.y}"
end
