require 'nokogiri'

module Providers::Deal972Lizamp

  class Wfs 
    
    output_value_gml = nil
    @@host_lizmap = "https://deal972.lizmap.com/carto/index.php"
    @@webclient = Providers::Webclient.new()
    @@logger = @@webclient.logger
      # url = "/lizmap/service?repository=0generale&project=CarteInteractiveDeal972"\
      #       "&SERVICE=WFS"\
      #       "&VERSION=1.3.0"\
      #       "&request=GetCapabilities"
    


    def self.features_in_bbox bbox_wfs_specs
      gml_xml_string = self.get_capabilities
      gml = Nokogiri::XML(gml_xml_string)
      result = gml.xpath(
        '/xmlns:WFS_Capabilities/xmlns:FeatureTypeList/xmlns:FeatureType' ,
        gml.namespaces
      )
      tarray = result.map do |i|
        i.xpath('xmlns:Name', gml.namespaces).first.text
      end
      cptr = Compteur.new(tarray.length)
      typenames_specs = tarray.join ','
      hresult = tarray.to_h { |x|
        [x, self.features_in_bbox_for_layer( x, bbox_wfs_specs, cptr)]
      }
      print "\r" + " " * cptr.msg.length
      print "\rResultat de la consultation des #{cptr.total} couches :\n"
      hresult
    end

    def self.features_in_bbox_for_layer layername, bbox_wfs_specs, cptr
      cptr.increment
      msg = "Avancement de la consultation des couches #{cptr.show} ..."
      cptr.msg = msg
      print "\r#{msg}"
      url = "/lizmap/service?"\
            "repository=0generale&project=CarteInteractiveDeal972"\
            "&SERVICE=WFS"\
            "&VERSION=1.3.0"\
            "&request=GetFeature"\
            "&srsName=EPSG:32620"\
            "&typeName=#{layername}" \
            "&bbox=#{bbox_wfs_specs}"
      url = @@host_lizmap+url
      @@logger.info "  >>>> " + url
      @@logger.info "  Fetching crossed on #{layername} layer ... "
      response = @@webclient.get(url)
      if response.code == "200"
        @@logger.info "Ok"
        #self.output_to_file response.body, $filepath_clipped_layers
        output_value_gml = response.body
      else
        @@logger.info "failed (#{response.code})"
      end
      # Pathname.new($filepath_clipped_layers).expand_path
      output_value_gml 
    end

    

    
  end
  
end
