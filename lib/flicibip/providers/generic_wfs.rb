require 'nokogiri'
require 'fileutils'


module Providers

  module WFS
    def self.GetCapabilities service 
      wfs = Providers::Generic::WFS.new()
      wfs.read service.build_query.GetCapabilities
    end
  end
  
end


module Providers::Generic

  class WFS
    
    def initialize 
      @webclient = Providers::Webclient.new()
      @logger = @webclient.logger
    end
    
    def request uri
      (code, response) = @webclient.get(uri)
      response
    end

    def read uri
      self.read_from_data self.request(uri), uri
    end

    def download uri, outputdir, with_timestamp=false, with_verb=false
      self.download_from_data(
        self.request(uri), uri, outputdir,
        with_timestamp, with_verb
      )
    end



    private 

    def read_from_data data, uri
      Readers::GML.read(
        uri.wfs_version,
        uri.request_type,
        data
      )
    end

    
    def download_from_data data, uri, outputdir, with_timestamp, with_verb
      info = self.read_from_data data, uri
      if uri.monolayer?
        perim = uri.target_layer.gsub(':','-')
      else
        perim = 'mutlilayers'
      end
      filename = "#{perim}.gml"
      if with_verb
        verb = uri.request_type
        filename = "#{verb}_" + filename
      end
      if with_timestamp
        timestamp = Time.now.strftime('%Y-%m-%d-%H-%M-%S')
        filename = "#{timestamp}_" + filename
      end
      FileUtils.mkdir_p(outputdir)
      @webclient.output_to_file data, File.join(outputdir,filename)
      info
    end

  end
end
module Providers::Flicibip
  
  class WFS
    def self.collect wfsquery, layers_list, outputdir, mode='single', action='download'
      client = WFS_ext.new(wfsquery)
      client.collect_from layers_list, outputdir, mode, action
    end
  end

  class WFS_ext
    
    def initialize wfsquery
      @query = wfsquery
      @client = Providers::Generic::WFS.new()
    end

    def collect_from layers_list, outputdir, mode, action
      if @query.wfs_version == '2.0.0' and mode != 'single'
        qry = @query.typeNames( layers_list.join ',' )
        if action == 'download'
          client.download qry, outputdir
        else
          client.request qry
        end
      else
        self.backport_collect_from layers_list, action, outputdir
      end
    end
    
    def backport_collect_from layers_list, action, outputdir
      cptr = Compteur.new(layers_list.length)
      hresult = layers_list.to_h { |layer|
        [
          layer,
          self.backport_collect_from_layer( layer, cptr, action, outputdir)]
      }
      print "\r" + " " * cptr.prvmsg.length
      print "\rResultat de la consultation des #{cptr.total} couches :\n"
      hresult_summary = hresult.compact
      if hresult_summary.length == 0
        puts "- aucune correspondance dans les couches interrogées"
      else
        hresult_summary.each{ |k,v|
          puts "- #{k}: #{v}"
        }
      end
      hresult
    end

    def backport_collect_from_layer layer, cptr, action, outputdir
      cptr.increment
      msg = "Avancement de la consultation des couches #{cptr.show} #{layer}  ..."
      if action == 'download'
        result = @client.download( @query.typeName(layer), outputdir )
      else
        @client.request( @query.typeName(layer) )
      end
      matched = result.values[0]
      msg = "#{msg} #{matched}"
      print "\r#{msg}".ljust(cptr.prvmsg.length)
      cptr.prvmsg = msg + "  "
      matched
    end
    
  end
end
