module Providers::Carmen

  class Wfs < Providers::Webclient    
    output_value_gml = nil
    @@host_carmen = "https://ws.carmen.developpement-durable.gouv.fr"
    
    def self.collect_clipped_features_gml bbox_wfs_specs
      nbr_couches_simultanees = 3
      gmls = []
      puts "  START :: Fetching WFS-Carmen  ... "
      Flicibip::Referentiel::get_typenames .each_slice(3){ |sl|
        gmls.push( self.dwd_clipped_features_gml_by_chunk bbox_wfs_specs, sl )
      }
      puts "  END :: Fetching WFS-Carmen."
      gmls
    end

    
    def self.dwd_clipped_features_gml_by_chunk bbox_wfs_specs, typenames_list
      typenames_specs = typenames_list.join(',')
      url = "/WFS/22/DEAL972_generale"\
            "?service=wfs"\
            "&version=2.0.0"\
            "&request=GetFeature"\
            "&srsName=EPSG:32620"\
            "&typeNames=#{typenames_specs}" \
            "&bbox=#{bbox_wfs_specs}"
      url = @@host_carmen+url
      # puts "  >>>> " + url
      print "  - crossed layers : #{typenames_specs} ... "
      response = self.get(url)
      if response.code == "200"
        puts "Ok"
        #self.output_to_file response.body, $filepath_clipped_layers
        output_value_gml = response.body
      else
        puts "failed (#{response.code})"
      end
      # Pathname.new($filepath_clipped_layers).expand_path
      output_value_gml 
    end


    def self.features_in_bbox bbox_wfs_specs
      nb =  Flicibip::Referentiel::get_typenames.length
      typenames_specs = Flicibip::Referentiel::tnlist_as_str
      url = "/WFS/22/DEAL972_generale"\
            "?service=wfs"\
            "&version=2.0.0"\
            "&request=GetFeature"\
            "&srsName=EPSG:32620"\
            "&typeNames=#{typenames_specs}" \
            "&bbox=#{bbox_wfs_specs}"
      url = @@host_carmen+url
      # puts "  >>>> " + url
      print "  Fetching crossed on #{nb} layers ... "
      response = self.get(url)
      if response.code == "200"
        puts "Ok"
        #self.output_to_file response.body, $filepath_clipped_layers
        output_value_gml = response.body
      else
        puts "failed (#{response.code})"
      end
      # Pathname.new($filepath_clipped_layers).expand_path
      output_value_gml 
    end
    
  end
  
end
