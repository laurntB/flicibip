require 'json'

module Providers::Cadastre

  # références :
  #
  #   - https://apicarto.ign.fr/api/doc/
  #   - https://geoservices.ign.fr/api-carto
  
  class Response < Providers::Response

    attr_reader :httpstatus, :respgeojson, :qry
    
    @respgeojson=nil
    @httpstatus=nil
    @qry=nil

    def initialize qry, status, geojson
      @respgeojson = Readers::RespJSON.new(geojson)
      @httpstatus = status
      @qry = qry
    end

    def matched?
      retval = false
      if @httpstatus == "200"
        if @respgeojson.numberMatched == 1
          retval = true
        end
      else
        retval = false
      end
      retval
    end
    
    def get_bounding_box 
      if self.matched?
        @respgeojson.bbox().join(',')
      end
    end

    def get_parcel_geom_as_json
      geom_str = nil
      if self.matched?
        if @respgeojson.numberMatched == 1
          geom = @respgeojson.json['features'][0]['geometry']
          geom_str = JSON.generate geom 
        end
      end
      if geom.nil?  then raise "bizarre" end
      geom_str
    end

    def get_rgeojson
      @respgeojson.json['features'][0]['geometry']
    end

    def get_feature
      @respgeojson.json['features'][0]
    end
  end
  
  class Rest 
    
    ## url query example : https://apicarto.ign.fr/api/cadastre/parcelle?code_insee=97230&section=0C&numero=0075
    @@host_apicadastre = "https://apicarto.ign.fr"
    @@webclient = Providers::Webclient.new()
    @@logger = @@webclient.logger
    
    def self.fetch_parcel_byPn parcelname
      url = "/api/cadastre/parcelle?"\
            "code_insee=#{parcelname.commune}&"\
            "section=#{parcelname.section}&"\
            "numero=#{parcelname.numero}"
      url = @@host_apicadastre+url
      @@logger.info "  Fetching parcel from ApiCadastre ... "
      (response_code, json) = @@webclient.get(url)
      Providers::Cadastre::Response.new(
        {
          classname: 'Cadastre::Rest',
          method: 'dwd_parcel_geojson',
          input: parcelname,
          url: url
        },
        response_code,
        json
      )
    end
    
  end

end
