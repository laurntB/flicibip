module Providers::GeoPlateforme

  class Wfs < Providers::Webclient
    output_value_gml = nil
    @@host_localgeoserver = "https://data.geopf.fr"
    
    def self.dwd_parcel_gml parcellename

      template = ERB.new <<-EOF
<?xml version="1.0" ?>
<GetFeature
    version="1.0.0"
    service="WFS"
    xmlns="http://www.opengis.net/wfs/2.0"
    xmlns:mq="https://data.geopf.fr/wfs/ows"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:fes="http://www.opengis.net/fes/2.0"
    xsi:schemaLocation="http://www.opengis.net/wfs/2.0 http://schemas.opengis.net/wfs/2.0.0/wfs.xsd">
  <Query typeNames="BDPARCELLAIRE-VECTEUR_WLD_BDD_WGS84G:parcelle"
	 srsName="EPSG:32620">
    <fes:Filter>
      <fes:PropertyIsEqualTo>
        <fes:ValueReference>code_dep</fes:ValueReference>
        <fes:Literal><%= id_parcellaire[0..1] %></fes:Literal>
        <fes:ValueReference>code_com</fes:ValueReference>
        <fes:Literal><%= id_parcellaire[2..4] %></fes:Literal>
        <fes:ValueReference>section</fes:ValueReference>
        <fes:Literal><%= id_parcellaire[9..10] %></fes:Literal>
        <fes:ValueReference>numero      </fes:ValueReference>
        <fes:Literal><%= id_parcellaire[11..14] %></fes:Literal>
      </fes:PropertyIsEqualTo>
    </fes:Filter>
  </Query>
</GetFeature>
EOF
      
      
      tmpfile = Tempfile.new('wfs_xml_qry')
      begin
        id_parcellaire = parcellename.as_id
        xmlqry = template.result(binding)
        p xmlqry
        tmpfile.write xmlqry
        tmpfile.rewind
        print "  Querying GeoPlateforme ... "
        response = self.post(
          @@host_localgeoserver+"/wfs/ows",
          tmpfile.path
        )
        if response.code == "200"
          puts "Ok"
          # self.output_to_file response.body, $filepath_parcelle_cible
          output_value_gml = response.body
        else
          puts "failed (#{response.code})"
        end
      ensure
        tmpfile.close
        tmpfile.unlink
      end
      # Pathname.new($filepath_parcelle_cible).expand_path
      output_value_gml 
    end

  end


end
