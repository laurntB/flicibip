

module Providers::GeoportailUrbanisme

  # documentation en ligne : https://api.gouv.fr/documentation/api_carto_gpu

  class Response < Providers::Response

    attr_reader :httpstatus, :respgeojson, :qry
    
    @respgeojson=nil
    @httpstatus=nil
    @qry=nil

    def initialize qry, status, geojson
      @respgeojson = Readers::RespJSON.new(geojson)
      @httpstatus = status
      @qry = qry
    end

    def matched?
      retval = false
      if @httpstatus == "200"
        if @respgeojson.numberMatched > 0 
          retval = true
        end
      else
        retval = false
      end
      retval
    end
    
    def mi
      @respgeojson.mi_byMthd @qry[:classname], @qry[:method]
    end

    def echo_mi
      @respgeojson.show_mi_byMthd @qry[:classname], @qry[:method]
    end
    
  end


  
  class Rest 

    @@src = "GPU"
    @@root_url = "https://apicarto.ign.fr/api/gpu/"
    @@webclient = Providers::Webclient.new()
    @@logger  =@@webclient.logger
    
    def self.zone_urba geom, partition
      self.generic_get 'zone-urba', geom, partition
    end

    def self.zone_urba_byPn pn
      self.generic_get_byPn 'zone-urba', pn
    end
    
    def self.zone_urba_byRgjsn rgjsn
      self.generic_get 'zone-urba', rgjsn.geom_str, rgjsn.partition
    end

    def self.prescription_surf geom, partition
      method = "prescription-surf"
      self.generic_get method, geom, partition
    end

    def self.prescription_surf_byPn pn
      self.generic_get_byPn 'prescription-surf', pn
    end

    def self.prescription_surf_byRgjsn rgjsn
      self.generic_get 'prescription-surf', rgjsn.geom_str, rgjsn.partition
    end


    
    private

    def self.build_url method, params
      qryparts = []
      params.each do |k,v|
        qryparts.push "#{k}=#{CGI.escape(v)}"            
      end
      @@root_url+method+"?"+qryparts.join("&")
    end

    
    def self.generic_get method, geom, partition
      url = self.build_url method, {geom: geom, partition: partition}
      @@logger.info "  Fetching on #{@src} (#{method}) ... "
      (code,response) = @@webclient.get(url)
      if code == "200"
        @@logger.info "Ok"
        output_value = response
      else
        @@logger.info "Failed"
      end
      Providers::GeoportailUrbanisme::Response.new(
        {
          classname: 'GeoportailUrbanisme',
          method: method,
          input: {geom: geom, partition: partition},
          url: url
        },
        code,
        output_value
      )
    end

    
    def self.generic_get_byPn method, pn
      resp2 = nil
      resp = Providers::Cadastre::Rest.fetch_parcel_byPn pn
      if resp.matched?
        partition = "DU_#{pn.commune}"
        geom = resp.get_parcel_geom_as_json
        resp2 = self.generic_get method, geom, partition
      else
        resp2 = Providers::GeoportailUrbanisme::Response.new(
          {
            classname: 'GeoportailUrbanisme',
            method: method,
            input: {pn: pn, partition: method, prevresp: resp},
            url: nil
          },
          "failed",
          resp
        )
      end
      if resp2.matched?
        resp2
      else
        raise @logger.info "Pas de données"
      end
    end
    
  end


end
