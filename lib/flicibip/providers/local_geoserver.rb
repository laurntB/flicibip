module Providers::LocalGeoserver

  class Wfs < Providers::Webclient

    output_value_gml = nil
    @@host_localgeoserver = "http://localhost:8080"
    
    def self.dwd_parcel_gml parcellename

      template = ERB.new <<-EOF
<?xml version="1.0" ?>
<GetFeature
    version="2.0.0"
    service="WFS"
    xmlns="http://www.opengis.net/wfs/2.0"
    xmlns:mq="http://localhost:8080/geoserver/mq/ows"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:fes="http://www.opengis.net/fes/2.0"
    xsi:schemaLocation="http://www.opengis.net/wfs/2.0 http://schemas.opengis.net/wfs/2.0.0/wfs.xsd">
  <Query typeNames="mq:cadastre-972-parcelles"
	 srsName="EPSG:32620">
    <fes:Filter>
      <fes:PropertyIsEqualTo>
        <fes:ValueReference>id</fes:ValueReference>
        <fes:Literal><%= id_parcellaire %></fes:Literal>
      </fes:PropertyIsEqualTo>
    </fes:Filter>
  </Query>
</GetFeature>
EOF
      
      
      tmpfile = Tempfile.new('wfs_xml_qry')
      begin
        id_parcellaire = parcellename.as_id
        tmpfile.write template.result(binding)
        tmpfile.rewind
        print "  Querying LocalGeoserver ... "
        response = self.post(
          @@host_localgeoserver+"/geoserver/mq/ows",
          tmpfile.path
        )
        if response.code == "200"
          puts "Ok"
          # self.output_to_file response.body, $filepath_parcelle_cible
          output_value_gml = response.body
        else
          puts "failed (#{response.code})"
        end
      ensure
        tmpfile.close
        tmpfile.unlink
      end
      # Pathname.new($filepath_parcelle_cible).expand_path
      output_value_gml 
    end

  end

end
