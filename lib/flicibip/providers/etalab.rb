require "zlib"
require "json"


$host_etalab = "https://cadastre.data.gouv.fr/data/etalab-cadastre"

module Providers::Etalab

  class Rest < Providers::Webclient

    
    def compose_url (commune)
      "/2023-10-01"\
      "/geojson/communes"\
      "/972"\
      "/#{commune}"\
      "/cadastre-#{commune}-parcelles.json.gz"
    end


    
    def get_parcelles_from_etalab_json (commune)
      hostname = URI($host_etalab).hostname
      uri_part = compose_url commune
      print "  Fetching json from #{hostname}  ... "
      response = fetch($host_etalab+uri_part)
      if response.code == "200"
        tmpfile = Tempfile.new('cadastre_geojson_gz')
        begin
          tmpfile.binmode
          tmpfile.write(response.body)
          tmpfile.flush
          tmpfile.close
          geojson = Zlib::GzipReader.open(tmpfile.path).read
          collection = RGeo::GeoJSON.decode(geojson)
        ensure
          tmpfile.close
          tmpfile.unlink
        end
        puts "Ok"
      else
        puts "failed (#{response.code})"
        abort "  >> " + (compose_url commune)
      end
      collection
    end

  end
end
