


module Readers
  
  class RespJSON

    attr_reader :json
    
    @json
    
    def initialize data
      @json = JSON.parse(data)
    end
    
    def properties
      @json['features'].map{ |feature|
        feature['properties']
      }
    end

    def numberMatched
      @json['numberMatched']
    end
    
    def bbox
      @json['bbox']
    end

    def mi_byMthd classname, method
      self.call_appropriate_method classname, method
    end
    
    def show_mi_byMthd classname, method
      mi = self.mi_byMthd classname, method
      puts "  .................................."
      puts "  Nombre d'éléments concernés : " + self.numberMatched.to_s
      mi.each do |o|
        puts "    - "+o
      end
    end

    private
    
    def call_appropriate_method classname, method
      prefix = "extract_mi"
      method_to_call = nil
      if classname == "GeoportailUrbanisme"
        provider = 'GPU'
        if method == "zone-urba"
          method_to_call = "#{prefix}_#{provider}_#{method.gsub('-','_')}"
        elsif method == "prescription-surf"
          method_to_call = "#{prefix}_#{provider}_#{method.gsub('-','_')}"
        end
      else
        raise "Oups no reader !"
      end
      self.send(method_to_call)
    end
    
    def extract_mi_GPU_zone_urba
      # {"gid"=>1113700
      #   "gpu_doc_id"=>"50a7dc1b58d6ce7115cdc5ea5bc438fd"
      #   "gpu_status"=>"production"
      #   "gpu_timestamp"=>"2021-10-27T16:03:38Z"
      #   "partition"=>"DU_97229"
      #   "libelle"=>"UL"
      #   "libelong"=>"UL : zone relative aux polarités littorales présentant une forte densité (Bourg, Anse Madame, Fond Lahaye et Fond Bernier) et une mixité des fonctions"
      #   "typezone"=>"U"
      #   "destdomi"=>nil
      #   "nomfic"=>"97229_reglement_20211019.pdf"
      #   "urlfic"=>nil
      #   "insee"=>nil
      #   "datappro"=>nil
      #   "datvalid"=>"20211019"
      #   "idurba"=>"97229_PLU_20211019"
      #   "idzone"=>nil
      #   "lib_idzone"=>nil
      # }
      self.properties.map do |item|
        "#{item['typezone']}(#{item['libelle']}) :: #{item['libelong']}"
      end
    end
    
    def extract_mi_GPU_prescription_surf
      # {"gid"=>3216311
      #   "gpu_doc_id"=>"50a7dc1b58d6ce7115cdc5ea5bc438fd"
      #   "gpu_status"=>"production"
      #   "gpu_timestamp"=>"2021-10-27T16:03:38Z"
      #   "partition"=>"DU_97229"
      #   "libelle"=>"Construction d'un viaduc en traversee à Fond Lahaye"
      #   "txt"=>"d6"
      #   "typepsc"=>"05"
      #   "nomfic"=>"97229_prescription_surf_05_00_20211019.pdf"
      #   "urlfic"=>nil
      #   "insee"=>nil
      #   "datappro"=>nil
      #   "datvalid"=>"20211019"
      #   "idurba"=>"97229_PLU_20211019"
      #   "stypepsc"=>"02"
      #   "idpsc"=>nil
      #   "lib_idpsc"=>nil
      # }
      self.properties.map do|item|
        "#{item['txt']} :: #{item['libelle']}"
      end
    end

  end
  
end
