

class Compteur

  attr_reader :avancement, :total, :prvmsg
  attr_writer :prvmsg
  
  def initialize total
    @total = total
    @avancement = 0
    @prvmsg = ""
  end

  def increment
    @avancement = @avancement + 1
  end
  
  def show
    "#{@avancement}/#{@total}"
  end
end
  
