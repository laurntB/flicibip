require 'tempfile'
require "pathname"
require "uri"
require "net/http"
require 'erb'



$filepath_parcelle_cible = "parcelle_cible.gml"
$filepath_clipped_layers = "clip_by_bbox.gml" 



module Providers
  
  class Webclient
    
    include Logging

    def initialize
      @http = nil
      @initialized = true
    end
    

    
    def net_http_options hostname
      pem_path = Pathname(__dir__)
                   .parent.parent + "data" + "cacert.pem"
      # p pem_path.to_s
      case hostname
      when 'localhost' then
        options = {}
      else
        options = {
          use_ssl: true,
          keep_alive_timeout: 30,
          ca_file: pem_path.to_s,
          verify_mode: OpenSSL::SSL::VERIFY_PEER
        }
      end
      options
    end

    
    
    def set_config_for_host hostname
      net_http_options(hostname).each do |k,v|
        @http.public_send("#{k}=", v)
      end
    end


    
    def build_http uri
      if uri.host == "localhost"
        @http = Net::HTTP.new(uri.host, uri.port)
      else
        @http = Net::HTTP.new(
          uri.host, uri.port,
          nil, nil, nil, nil,
          'localhost:8080'
        )
      end
      self.set_config_for_host(uri.host)
    end

    
    
    def post(url, datafile = nil, limit=10)
      raise ArgumentError, 'HTTP redirect too deep' if limit == 0
      uri = URI.parse(url)
      self.build_http uri
      req = Net::HTTP::Post.new(uri.request_uri)
      req.body = File.read(datafile)
      req.content_type = 'text/xml'
      response =  @http.start { |http|
        @http.request(req)
      }
      case response
      when Net::HTTPSuccess     then response
      when Net::HTTPRedirection then
        post(response['location'],datafile, limit - 1)
      else
        response.error!
      end
    end



    def get(input_uri)
      uri = URI.parse(input_uri)
      limit = 10
      output = nil
      logger.info "url> " + uri.to_s
      # You should choose better exception.
      raise ArgumentError, 'HTTP redirect too deep' if limit == 0
      req = Net::HTTP::Get.new(uri)
      response = Net::HTTP.start(
        uri.host,
        self.net_http_options(uri.host)
      ) { |http|
        http.request(req)
      }
      case response
      when Net::HTTPSuccess     then response
      when Net::HTTPRedirection 
        logger.info "loc> " + response['location']
        fetch(response['location'], limit - 1)
      else
        response.error!
      end
      logger.info "  Web Request ... "
      if response.code == "200"
        logger.info "Ok"
      else
        logger.info "failed (#{response.code})"
      end
      [response.code, response.body]
    end


    
    def output_to_file data, filename
      logger.info "  Writing #{filename} file ... "
      File.open(filename,"w"){|file|
        file.write(data)
      }
      logger.info "Ok."
    end

  end   


  class Response
    include Logging    
  end
  
end

require 'flicibip/providers/geomartinique'
require 'flicibip/providers/deal972lizmap'
require 'flicibip/providers/cadastre'
require 'flicibip/providers/geoportail_urbanisme'
require 'flicibip/providers/generic_wfs'
# require 'flicibip/providers/carmen' service désactivé été 2024

