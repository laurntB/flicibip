class Rgjsn
  @json
  def initialize rjson
    @json = rjson
  end

  def bbox_str
    astr = @json['bbox'].map do |e|
      e.to_s
    end
    astr.join(',')
  end
end

class RgjsnFeature 
  
  def initialize rjson
    @json = rjson
  end

  def geom_str
    JSON.generate(@json['geometry'])
  end

  def partition
    props = @json['properties']
    "DU_#{props['code_dep']}#{props['code_com']}"
  end

  def bbox_str
    astr = @json['bbox'].map do |e|
      e.to_s
    end
    astr.join(',')
  end
end
