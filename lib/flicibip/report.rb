

class Report
  
  def initialize rdr_gml
    @matched = rdr_gml.matched
    @checklist_response = rdr_gml.checklist_response
  end
  
  def html
    result = @checklist_response
    html = [
      '<h3>Synthèse</h3>',
      '<p>'
    ]
    if @matched.size == 0
      html += [
        "Parcelle \"libre\" en première approche. Elle n'intercepte aucune des contraintes environnementales étudiées."
      ]
    end
    if @matched.size == 1
      html += [
        "La parcelle est vraissemblablement concernée par le zonage :  #{@matched.keys[0]}."
      ]
    end
    if @matched.size > 1
      html += [
        "La parcelle est vraissemblablement concernée par #{@matched.size} zonages :  #{@matched.keys.join(', ')}."
      ]
    end
    html += [
      '</p>',
      '<div class="fr-table fr-table--bordered fr-table--layout-fixed">',
      '<table>',
      '<caption>Détails du requêtage de la base de données geographiques</caption>'
    ]
    html+=[
      '<thead>',
      '<tr>',
      '<th scope="col">Libellé des zonage particuliers</th>',
      '<th scope="col">À vérifier</th>',
      '<th scope="col">Nombre éventuel de prescriptions</th>',
      '</tr>',
      '</thead>',
      '<tbody>',
    ]
    result.each do |k,v|
      interceptee = v[:nbr].nil? ? "" : "X"
      html+=[
        '<tr>',
        "<td>#{v[:libelle]}</td><td>#{interceptee}</td><td>#{v[:nbr]}</td>",
        '</tr>'
      ]
    end
    html+=[
      '</tbody>',
      '</table>',
      '</div>'
    ]
    html.reduce(&:+)
  end


  def txt
    result = @checklist_response
    txt = [
      "\n",
      "Résultats : \n"
    ]
    result.each do |k,v|
      indicateur = v[:nbr].nil? ? " " : v[:nbr].to_s
      txt+= ["    - [#{indicateur}]   #{v[:libelle].ljust(35, ' ')}  (#{k})\n"]
    end
    txt += ["\n"]
    txt.reduce(&:+)
  end

end

class ReportMi
  
  def initialize type, array
    @type = type
    @array = array
  end

  
  def html
    out = [
      "<h1>#{@type}</h1>",
      '<ul>'
    ]
    @array.each do |item|
      out.push( '<li>'+item+'</li>' )
    end
    out.push '</ul>'
    out.join('')
  end
end
