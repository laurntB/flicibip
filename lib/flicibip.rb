require "yaml"

require 'version'

require 'flicibip/parcelle'
require 'flicibip/avcptr'
require 'flicibip/uri'
require 'flicibip/gml'
require 'flicibip/json'
require 'flicibip/rgeo'
require 'flicibip/report'
require 'flicibip/rgjsn'
require 'flicibip/referentiel'
require 'flicibip/logging'
require 'flicibip/providers'




module Flicibip

  module_function

  def cli_collect_intersected_feature_by_parcel cogcom5c, section, numero, options
    perim = YAML.load_file(options[:'data-source'])
    service = Flicibip::WfsService.new(
      perim['service']['url'],
      perim['service']['version']
    )
    layers_list =   perim['couches']

    parcelle_cible = ParcelleName.new(cogcom5c, section, numero) # "97106", "AR", "363"
    resp = Providers::Cadastre::Rest.fetch_parcel_byPn parcelle_cible
    if resp.matched?
      bbox = reproject_bounds resp.get_bounding_box
      query = service.build_query()
	        .GetFeature
	        .bbox(bbox)
      Providers::Flicibip::WFS.collect query, layers_list, options[:'output-dir']
      puts "GML files available in directory : #{options[:'output-dir']}"
    end
   end

  # def cli_qry_deal972lizmap cogcom5c, section, numero
  #   parcelle_cible = ParcelleName.new(cogcom5c, section, numero)
  #   resp = Providers::Cadastre::Rest.fetch_parcel_byPn parcelle_cible
  #   if resp.matched?
  #     bbox = reproject_bounds resp.get_bounding_box
  #     hresult = Providers::Deal972Lizamp::Wfs.features_in_bbox bbox 
  #     hresult.each do |key, value|
  #       d = Readers::GML.new(value)
  #       nb = d.docxml.xpath('//gml:featureMember', d.docxml.namespaces).length
  #       if nb > 0
  #         puts "- [#{nb}] #{key}"
  #       end
  #     end
  #   else
  #     if resp.httpstatus == "200"
  #       puts "  Check again, seem that this parcel reference does not exist in the current edition"
  #     else
  #       puts "  The request to provider::cadastre as returned a status code of #{resp.httpstatus}"
  #     end
  #   end
  # end

  # def cli_qry_geoportail cogcom5c, section, numero
  #   parcelle_cible = ParcelleName.new(cogcom5c, section, numero)
  #   resp = Providers::Cadastre::Rest.fetch_parcel_byPn parcelle_cible
  #   if resp.matched?
  #     resp2 = Providers::GeoportailUrbanisme::Rest.zone_urba_byPn parcelle_cible
  #     resp2.echo_mi
  #   else
  #     if resp.httpstatus == "200"
  #       puts "  Check again, seem that this parcel reference does not exist in the current edition"
  #     else
  #       puts "  The request to provider::cadastre as returned a status code of #{resp.httpstatus}"
  #     end
  #   end
  # end

  def cli_qry_wfs_capabilities url, options
    service = Flicibip::WfsService.new(url)
    summary = Providers::WFS.GetCapabilities service
    YAML.dump(summary)
  end
  
  # def interroge_lizmap cogcom5c, section, numero
  #   gml = nil # retval
  #   parcelle_cible = ParcelleName.new(cogcom5c, section, numero)
  #   resp = Providers::Cadastre::Rest.fetch_parcel_byPn parcelle_cible
  #   if resp.matched?
  #     bbox = reproject_bounds resp.get_bounding_box
  #     gml = Providers::Lizmap::Wfs.features_in_bbox bbox 
  #   # puts Report.new(Readers::GML.new(gml)).txt 
  #   else
  #     if resp.httpstatus == "200"
  #       puts "  Check again, seem that this parcel reference does not exist in the current edition"
  #     else
  #       puts "  The request to provider::cadastre as returned a status code of #{resp.httpstatus}"
  #     end
  #   end
  #   gml
  # end


  
  def cli_interroge_geoportail cogcom5c, section, numero
    parcelle_cible = ParcelleName.new(cogcom5c, section, numero)
    resp = Providers::Cadastre::Rest.fetch_parcel_byPn parcelle_cible
    if resp.matched?
      resp2 = Providers::GeoportailUrbanisme::Rest.zone_urba_byPn parcelle_cible
      ### resp2.show_mi('GPU', 'urba-zone',retval[0])
      resp2.echo_mi
    else
      if resp.httpstatus == "200"
        puts "  Check again, seem that this parcel reference does not exist in the current edition"
      else
        puts "  The request to provider::cadastre as returned a status code of #{resp.httpstatus}"
      end
    end
  end


  # def fetch_parcel pn
  #   retval = nil
  #   resp = Providers::Cadastre::Rest.fetch_parcel_byPn pn
  #   if resp.matched?
  #     retval = resp
  #   else
  #     if resp.httpstatus == "200"
  #       puts "  Check again, seem that this parcel reference does not exist in the current edition"
  #       retval = Providers::Cadastre::Response.new(
  #         resp.httpstatus,
  #         {pn: pn},
  #         '{"numberMatched": 0}'
  #       )
  #     else
  #       puts "  The request to provider::cadastre as returned a status code of #{resp.httpstatus}"
  #       retval = Providers::Cadastre::Response.new(
  #         resp.httpstatus,
  #         {pn: pn},
  #         '{}'
  #       )
  #     end
  #   end
  #   retval
  # end
  
  
  def discoror_byRgjsn rgeojson 
    data = []
    gjsn = RgjsnFeature.new(rgeojson)
    resp1 = Providers::GeoportailUrbanisme::Rest.zone_urba_byRgjsn gjsn
    if resp1.matched?
      data.push ReportMi.new('zonage',resp1.mi).html
    end
    resp2 = Providers::GeoportailUrbanisme::Rest.prescription_surf_byRgjsn gjsn
    if resp2.matched?
      data.push ReportMi.new('SUP',resp2.mi).html
    end
    {
      status: 0,
      report: data.join("<br/><br/>\n\n            ")
    }
  end

  
  def discoror_byParcref parcelle_string_desc
    
    parts = parcelle_string_desc.split('-')
    parcellename = ParcelleName.new(parts[0],parts[1],parts[2])

    resp = fetch_parcel parcellename
    if resp.matched?
      rgeojson = resp.get_feature
      discoror_byRgjsn rgeojson 
    else
      {
        status: -9,
        report: "Error: pas de parcelle"
      }
    end
  end
  
end

